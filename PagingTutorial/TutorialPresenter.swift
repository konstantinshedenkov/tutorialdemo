//
//  TutorialPresenter.swift
//  InPlaces
//
//  Created by Konstantin Shendenkov on 19.09.17.
//  Copyright © 2017 Incode2015. All rights reserved.
//

import Foundation

protocol TutorialView: class {
  
  func scrollTo(_ page: Int)
  func show(_ page: Int)
  func showNumberOfPages(_ pages: Int)
  func showNextAction(title: String)
  func btnSkip(hidden: Bool)
  
  func tutorialComplete()
}

protocol TutorialPresenter {
  
  var numberOfPages: Int { get }
  
  func viewWillAppear()
  
  func btnSkipPressed()
  func btnNextPressed()
  
  func configure(cell: TutorialCell, for index: Int)
  func tutorialDidScrolled(withCenterIn item: Int)
}

class TutorialPresenterImplementation {
  
  fileprivate weak var view: TutorialView?
  
  fileprivate let numOfPages = TutorialConstants.numberOFTutorialPages
  
  var currentPage: Int = 0 {
    didSet {
      let isLastPage = currentPage == (numOfPages - 1)
      
      view?.show(currentPage)
      view?.showNextAction(title: isLastPage ? "Start" : "Next")
      view?.btnSkip(hidden: isLastPage)
    }
  }
  
  //MARK: - Init
  init(view: TutorialView) {
    self.view = view
  }
  
  // MARK: -
  fileprivate func completeAction() {
    print("Complete!")
//    didTutorialComplete?()
//    tutorialView?.tutorialComplete()
  }
}

//MARK: -

extension TutorialPresenterImplementation: TutorialPresenter {
  var numberOfPages: Int {
    return numOfPages
  }
  
  func viewWillAppear() {
    view?.showNumberOfPages(numOfPages)
  }
  
  func btnSkipPressed() {
    completeAction()
  }
  
  func btnNextPressed() {
    let isLastPage = currentPage == numOfPages - 1
    if isLastPage {
      completeAction()
    } else {
      view?.scrollTo(currentPage + 1)
    }
  }
  
  func configure(cell: TutorialCell, for index: Int) {
    let localeStr = Locale.getMainLanguage()
    cell.imageName = "tutorialPage\(index)\(localeStr)"
  }
  
  func tutorialDidScrolled(withCenterIn item: Int) {
    currentPage = item
  }
}














