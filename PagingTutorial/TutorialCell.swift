//
//  TutorialCell.swift
//  InPlaces
//
//  Created by Konstantin Shendenkov on 20.07.17.
//  Copyright © 2017 Incode2015. All rights reserved.
//

import UIKit

class TutorialCell: UICollectionViewCell {
  
  @IBOutlet weak var imgPage: UIImageView!
  
  var imageName: String? {
    didSet {
      if let name = imageName, let image = UIImage(named: name) {
        imgPage.image = image
      }
    }
  }
  
}
