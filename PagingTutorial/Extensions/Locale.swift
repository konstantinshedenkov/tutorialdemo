//
//  Locale.swift
//  InPlaces
//
//  Created by Konstantin Shendenkov on 19.09.17.
//  Copyright © 2017 Incode2015. All rights reserved.
//

import Foundation

extension Locale {
  
  // Return first of device preffered language.
  // Default - en
  //
  static func getMainLanguage() -> String {
    
    let defaultLanguage = "en"
    for language in preferredLanguages {
      let arr = language.components(separatedBy: "-")
      
      guard let language = arr.first else { return defaultLanguage }
      return language
    }
    
    return defaultLanguage
  }
}
