//
//  TutorialConstants.swift
//  PagingTutorial
//
//  Created by iDeveloper on 26.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import Foundation

enum TutorialConstants {
  
  enum nib: String {
    case tutorialCell = "TutorialCell"
  }
  
  static let numberOFTutorialPages = 4
}
