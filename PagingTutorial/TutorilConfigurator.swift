//
//  TutorilConfigurator.swift
//  PagingTutorial
//
//  Created by iDeveloper on 26.01.2018.
//  Copyright © 2018 iDeveloper. All rights reserved.
//

import Foundation

protocol TutorialConfigurator {
  static func configure(_ tutorialController: TutorialViewController)
}

class TutorialConfiguratorImplementation {
  static func configure(_ tutorialController: TutorialViewController) {
    let presenter = TutorialPresenterImplementation(view: tutorialController)
    tutorialController.presenter = presenter
  }
}
