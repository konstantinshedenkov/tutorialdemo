//
//  TutorialViewController.swift
//  InPlaces
//
//  Created by Konstantin Shendenkov on 20.07.17.
//  Copyright © 2017 Incode2015. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {
  
  var presenter: TutorialPresenter?
  
  // MARK: - IBOutlets
  @IBOutlet weak var pageControl: UIPageControl!
  @IBOutlet weak var collectionView: UICollectionView!
  
  @IBOutlet weak var btnSkip: UIButton!
  @IBOutlet weak var btnNext: UIButton!
  
  // MARK: - UIViewController overrides
  override func viewDidLoad() {
    super.viewDidLoad()
    TutorialConfiguratorImplementation.configure(self)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    presenter?.viewWillAppear()
  }
  
  // MARK: - IBActions
  @IBAction func btnSkipPressed() {
    presenter?.btnSkipPressed()
  }
  
  @IBAction func btnNextPressed() {
    presenter?.btnNextPressed()
  }
}

//
// MARK: - TutorialView
//

extension TutorialViewController: TutorialView {
  
  func scrollTo(_ page: Int) {
    let point = CGPoint(x: CGFloat(page) * collectionView.bounds.width,
                        y: collectionView.contentOffset.y)
    collectionView.setContentOffset(point, animated: true)
  }
  
  func show(_ page: Int) {
    pageControl.currentPage = page
  }
  
  func showNumberOfPages(_ pages: Int) {
    pageControl.numberOfPages = pages
  }
  
  func showNextAction(title: String) {
    btnNext.setTitle(title, for: .normal)
  }
  
  func btnSkip(hidden: Bool) {
    btnSkip.isHidden = hidden
  }
  
  func tutorialComplete() {
    btnNext.isEnabled = false
    btnSkip.isEnabled = false
    
    dismiss(animated: true, completion: nil)
  }
}

// MARK: - UICollectionViewDataSource
extension TutorialViewController: UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return presenter?.numberOfPages ?? 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TutorialConstants.nib.tutorialCell.rawValue, for: indexPath) as! TutorialCell
    presenter?.configure(cell: cell, for: indexPath.item)
    return cell
  }
}

// MARK: - UICollectionViewDelegate
extension TutorialViewController: UICollectionViewDelegate {
  public func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2),
                         y: (scrollView.frame.height / 2))
    
    if let ip = collectionView.indexPathForItem(at: center) {
      presenter?.tutorialDidScrolled(withCenterIn: ip.item)
    }
  }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension TutorialViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    return collectionView.bounds.size
  }
}




